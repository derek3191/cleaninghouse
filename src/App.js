import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import Navbar from "./components/navbar.component";
import RoomList from "./components/room-list.component";
import EditRoom from "./components/edit-room.component";
import CreateRoom from "./components/create-room.component";
import UserList from "./components/user-list.component";

function App() {
  return (
    <Router>
      {/* Conatiner div added to give space on the sides */}
      <div className="container">
        <Navbar />
        <br />
        <Route path="/" exact component={RoomList} />
        <Route path="/edit/:id" component={EditRoom} />
        <Route path="/create/" component={CreateRoom} />
        <Route path="/user" component={UserList} />
      </div>
    </Router>
  );
}

export default App;
