import React, { Component } from "react";
import axios from "axios";

class RoomList extends Component {
  constructor(props) {
    super(props);

    this.getRooms = this.getRooms.bind(this);

    this.state = {
      rooms: [],
      content: ""
    };
    //        this.state.rooms = this.getRooms();
    this.getRooms().then(data => {
      console.log(data.length);
      this.setState({
        rooms: data
      });
      console.log(this.state.rooms[0].description);
    });
    console.log(`This is the data\n ${this.state.rooms}`);

    // this.state.content = this.state.rooms.map((room) =>
    //   <div>
    //       <p>{room.id}</p>
    //       <p>{room.name}</p>
    //       <p>{room.description}</p>
    //   </div>
    // );
  }

  getRooms() {
    return axios
      .get("http://localhost:5000/rooms")
      .then(function(response) {
        console.log(response.data);
        return response.data;
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        There are {this.state.rooms.length} rooms:
        <br />
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Num of Items</th>
            </tr>
          </thead>
          <tbody>
            {this.state.rooms.map(room => (
              <tr>
                <td>{room.name}</td>
                <td>{room.description}</td>
                <td>{room.items.length}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default RoomList;
