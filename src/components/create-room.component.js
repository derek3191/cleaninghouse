import React, { Component } from "react";
import axios from "axios";

class CreateRoom extends Component {
  constructor(props) {
    super(props);

    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      name: "",
      description: "",
      items: [],
      username: "",
      users: []
    };
  }

  componentDidMount() {
    this.setState({
      users: ["derek", "kt"],
      username: "derek"
    });
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }
  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    });
    console.log(`Description changed to ${this.state.description}`);
  }
  onChangeName(e) {
    this.setState({
      name: e.target.value
    });
    console.log(`Name changed to ${this.state.name}`);
  }

  onSubmit(e) {
    e.preventDefault();
    console.log(this);
    const room = {
      name: this.state.name,
      description: this.state.description,
      items: this.state.items
    };

    axios
      .post("http://localhost:5000/rooms/add", room)
      .then(function(response) {
        alert("SAVED!");
        console.log(response);
      })
      .catch(function(error) {
        alert("Something went wrong!");
        console.log(error);
      });

    console.log(`The following room was saved\n ${room}`);

    window.location = "/";
  }

  render() {
    return (
      <div>
        <h3>Create Room</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Username: </label>
            <select
              ref="userInput"
              required
              className="form-control"
              value={this.state.username}
              onChange={this.onChangeUsername}
            >
              {this.state.users.map(function(user) {
                return (
                  <option key={user} value={user}>
                    {user}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="form-group">
            <label>Name: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.name}
              onChange={this.onChangeName}
            />
          </div>
          <div className="form-group">
            <label>Description: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.description}
              onChange={this.onChangeDescription}
            />
          </div>

          <div className="form-group">
            <input type="submit" value="Add Room" className="btn btn-primary" />
          </div>
        </form>
      </div>
    );
  }
}

export default CreateRoom;
