const router = require("express").Router();
let Room = require("../models/room.model");

router.route("/").get((req, res) => {
  Room.find()
    .then(rooms => res.json(rooms))
    .catch(err => res.status(400).json("Error: " + err));
});

router.route("/add").post((req, res) => {
  const name = req.body.name;
  const description = req.body.description;
  const items = req.body.items;

  const newRoom = new Room({
    name,
    description,
    items
  });

  newRoom
    .save()
    .then(() => res.json("Room added!"))
    .catch(err => res.status(400).json("Error: " + err));
});

router.route("/:id").get((req, res) => {
  Room.findById(req.params.id)
    .then(room => res.json(room))
    .catch(err => res.status(400).json("Error: " + err));
});

router.route("/delete/:id").delete((req, res) => {
  Room.findByIdAndDelete(req.params.id)
    .then(() => res.json("Room deleted."))
    .catch(err => res.status(400).json("Error: " + err));
});

router.route("/update/:id").post((req, res) => {
  Room.findByIdAndUpdate(req.params.id)
    .then(room => {
      room.name = req.body.name;
      room.description = req.body.description;
      room.items = req.body.items;

      room
        .save()
        .then(() => res.json("Room updated."))
        .catch(err => res.status(400).json("Error: " + err));
    })
    .catch(err => res.status(400).json("Error: " + err));
});

module.exports = router;
