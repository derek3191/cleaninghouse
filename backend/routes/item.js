const router = require("express").Router();
let Item = require("../models/item.model");

router.route("/").get((req, res) => {
  Item.find()
    .then(items => res.json(items))
    .catch(err => res.status(400).json("Error: " + err));
});

router.route("/add").post((req, res) => {
  const name = req.body.name;
  const cleaning = req.body.cleaning;
  const cleaningFrequency = req.body.cleaningFrequency;
  const lastCleanedBy = req.body.lastCleanedBy;
  const lastCleanedDate = req.body.lastCleanedDate;

  const newItem = new Item({
    name,
    cleaning,
    cleaningFrequency,
    lastCleanedBy,
    lastCleanedDate
  });

  newItem
    .save()
    .then(() => res.json("Item added!"))
    .catch(err => res.status(400).json("Error: " + err));
});

module.exports = router;
