const mongoose = require("mongoose");
const ItemSchema = require("./item.model");

const Schema = mongoose.Schema;

const roomSchema = new Schema(
  {
    name: { type: String, required: true },
    description: { type: String, required: true },
    items: { type: [ItemSchema] }
  },
  {
    timestamps: true
  }
);

const Room = mongoose.model("Room", roomSchema);

module.exports = Room;
