const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ItemSchema = new Schema(
  {
    name: { type: String, required: true },
    cleaning: { type: [String], required: true },
    cleaningFrequency: { type: String, required: true },
    lastCleanedBy: { type: String },
    lastCleanedDate: { type: Date }
  },
  {
    timestamps: true
  }
);

const Item = mongoose.model("Item", ItemSchema);

module.exports = Item;
module.exports = ItemSchema;
